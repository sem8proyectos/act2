
package actividad2;

/* Imports */
// import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.File;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;


public class Actividad2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        eliminarHTML();
    }
    
    /**
     * 
     * Crear Archivo log
     * @param contenido 
     */
    public static void crearArchivo(String contenido){
        try {
            String ruta = "src/log/log_archivos.txt";
            // String contenido = "Contenido de ejemplo";
            File file = new File(ruta);
            // Si el archivo no existe es creado
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file);
            try (BufferedWriter bw = new BufferedWriter(fw)) {
                bw.write(contenido);
            }
        }catch (IOException ex) {
            System.out.println(ex);
        }
    }
    
    /**
     * Barre la carpeta de archivos para eliminar el HTML y crea el archivo log mediante un llamado a la funcion crearArchivo().
     */
    public static void eliminarHTML() {
        double tiempoInicio = System.currentTimeMillis();
        String rutaFiles = "src/Files";
        File folder = new File(rutaFiles);
        File[] listOfFiles = folder.listFiles();
        String lineas = "";
        String contenido = "";
        String[] etiquetas = {"<html>", "</html>"};
        double tiempoTotal = 0;
        for (File listOfFile : listOfFiles) {
            if (listOfFile.isFile()) {
                // System.out.println("open_file " + listOfFile.getName());
                double tiempoInicioEliminar = System.currentTimeMillis();
                try {
                    // Desktop.getDesktop().open(listOfFile);
                    //Construct the new file that will later be renamed to the original filename.
                    File tempFile = new File(listOfFile.getAbsolutePath() + ".tmp");
                    FileWriter fw = new FileWriter(listOfFile);                    
                    FileReader fr = new FileReader(listOfFile); 
                    BufferedReader br = new BufferedReader(fr);
                    PrintWriter pw = new PrintWriter(fw);
                    while ((lineas = br.readLine()) != null) {
                        for (String etiqueta : etiquetas) {
                            if (!lineas.trim().equals(etiqueta)) {
                                pw.println(lineas);
                                pw.flush();
                            }
                        }
                    }
                    br.close();
                    pw.close();
                    //Delete the original file
                    if (!listOfFile.delete()) {
                        System.out.println("No se pudo borrar el archivo.");
                        return;
                    }

                    //Rename the new file to the filename the original file had.
                    if (!tempFile.renameTo(listOfFile)){
                        System.out.println("No se pudo renombrar el archivo.");

                    }
                }catch (IOException ex) {
                    System.out.println(ex);
                }
                double tiempoFinalEliminar = System.currentTimeMillis() - tiempoInicioEliminar;
                contenido += listOfFile.getAbsolutePath() + "\t" + (tiempoFinalEliminar / 1000) + "\n";
                tiempoTotal += tiempoFinalEliminar;
            } else if (listOfFile.isDirectory()) {
                // System.out.println("Directory " + listOfFile.getName());
            }
        }
        contenido += "\n \n";
        contenido += "Tiempo total en eliminar etiquetas HTML: " + (tiempoTotal / 1000) + "\n";
        double tiempoFinal = System.currentTimeMillis() - tiempoInicio;
        contenido += "Tiempo total de ejecución: " + (tiempoFinal / 1000) + "\n";
        crearArchivo(contenido);
    }
    
}
